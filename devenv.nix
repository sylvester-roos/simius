{
  pkgs,
  lib,
  config,
  inputs,
  ...
}:

{
  dotenv = {
    enable = true;
    filename = ".env.dev";
  };

  # https://devenv.sh/basics/
  # env.GREET = "devenv";

  # https://devenv.sh/packages/
  packages = with pkgs; [
    git
    erlang_27
  ];

  # https://devenv.sh/languages/
  # languages.rust.enable = true;

  # https://devenv.sh/processes/
  processes = {
    run-bot = {
      exec = "mix run --no-halt";
      # process-compose = {
      # };
    };
  };

  # https://devenv.sh/services/
  # services.postgres.enable = true;

  # https://devenv.sh/scripts/
  scripts = {
    build-image.exec = # bash
      ''nix run .#container.copyToPodman'';
  };

  # enterShell = ''
  #   hello
  #   git --version
  # '';

  # https://devenv.sh/tasks/
  # tasks = {
  #   "myproj:setup".exec = "mytool build";
  #   "devenv:enterShell".after = [ "myproj:setup" ];
  # };

  # https://devenv.sh/tests/
  # enterTest = ''
  #   echo "Running tests"
  #   git --version | grep --color=auto "${pkgs.git.version}"
  # '';

  # See full reference at https://devenv.sh/reference/options/
}
