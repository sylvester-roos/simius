defmodule Simius do
  @moduledoc """
  Documentation for `Simius`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Simius.hello()
      :world

  """
  def hello do
    :world
  end
end
