defmodule Simius.VentingClearer do
  use GenServer

  require Logger

  alias Simius.Persistence
  alias Nostrum.Api

  defp venting_channel_id do
    Application.fetch_env!(:simius, :venting_channel_id)
    |> String.to_integer()
  end

  # Client

  def start_link(_args) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def new_timer do
    GenServer.cast(__MODULE__, :new_timer)
  end

  # Server

  @impl true
  def init(_args) do
    timer_ref = clear_interval()
    {:ok, %{timer: timer_ref}}
  end

  @impl true
  def handle_info(:clear, _state) do
    clear_channel()
    timer_ref = clear_interval()
    {:noreply, %{timer: timer_ref}}
  end

  @impl true
  def handle_cast(:new_timer, %{timer: timer_ref}) do
    if not is_nil(timer_ref) do
      Process.cancel_timer(timer_ref)
      Logger.info("Existing timer has been cancelled")
    end

    timer_ref = clear_interval()
    # immediately clear the channel when a timer has been created
    clear_channel()

    {:noreply, %{timer: timer_ref}}
  end

  # Helpers

  def formatted_interval do
    interval = Persistence.get_interval()

    seconds_total = div(interval, 1000)
    days = div(seconds_total, 86400)
    hours = div(rem(seconds_total, 86400), 3600)
    minutes = div(rem(seconds_total, 3600), 60)
    seconds = rem(seconds_total, 60)

    days_str = if days == 1, do: "day", else: "days"
    hours_str = if hours == 1, do: "hour", else: "hours"
    minutes_str = if minutes == 1, do: "minute", else: "minutes"
    seconds_str = if seconds == 1, do: "second", else: "seconds"

    [
      if(days > 0, do: "#{days} #{days_str}", else: nil),
      if(hours > 0, do: "#{hours} #{hours_str}", else: nil),
      if(minutes > 0, do: "#{minutes} #{minutes_str}", else: nil),
      if(seconds > 0, do: "#{seconds} #{seconds_str}", else: nil)
    ]
    |> Enum.reject(&is_nil/1)
    |> Enum.join(", ")
  end

  defp clear_interval do
    case Persistence.get_interval() do
      nil ->
        nil

      interval ->
        timer_ref = Process.send_after(__MODULE__, :clear, interval)
        Logger.info("Saved a timer with an interval of #{formatted_interval()}")

        timer_ref
    end
  end

  defp clear_channel do
    channel_id = venting_channel_id()
    message_ids = Api.get_channel_messages!(channel_id, :infinity) |> Enum.map(& &1.id)

    case message_ids do
      [single_id] -> Api.delete_message!(channel_id, single_id)
      [] -> nil
      ids -> Api.bulk_delete_messages!(channel_id, ids)
    end

    time_parts = formatted_interval()

    Api.create_message!(
      channel_id,
      "🧹 Channel has been cleared. Next clear will happen in #{time_parts}."
    )

    :ok
  end
end
