defmodule Simius.Persistence do
  use GenServer

  require Logger

  @table :simius_storage

  # Client

  def start_link(_args) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  # Server

  @impl true
  def init(_args) do
    Process.flag(:trap_exit, true)

    table_dir = Path.join([:code.priv_dir(:simius), "dets"])
    table_path = Path.join([table_dir, Atom.to_string(@table)])

    File.mkdir_p!(table_dir)

    :dets.open_file(@table, file: String.to_charlist(table_path), type: :set)
  end

  @impl true
  def handle_info({:EXIT, _from, _reason}, state) do
    exit()
    {:noreply, state}
  end

  @impl true
  def terminate(_reason, _state) do
    exit()
  end

  # Helpers

  def save_interval(interval) do
    :dets.insert(@table, {:clear_interval, interval})
  end

  def get_interval do
    case :dets.lookup(@table, :clear_interval) do
      [{:clear_interval, interval}] -> interval
      [] -> nil
    end
  end

  defp exit do
    :dets.close(@table)
    Logger.info("DETS table closed")
  end
end
