defmodule Simius.Consumer do
  use Nostrum.Consumer

  require Logger

  alias Simius.Commands
  alias Nostrum.Struct.Interaction
  alias Simius.Consumer.Ready

  def handle_event({:READY, data, _ws_state}) do
    Ready.handle(data)
  end

  def handle_event({:MESSAGE_CREATE, %{author: %{bot: nil}}, _ws_state}) do
    :noop
  end

  def handle_event({:INTERACTION_CREATE, interaction, _ws_state}) do
    handle_interaction_creation(interaction)
  end

  def handle_event(_event), do: :noop

  defp handle_interaction_creation(%Interaction{data: %{name: "echo"}} = interaction) do
    Commands.Echo.handle(interaction)
  end

  defp handle_interaction_creation(%Interaction{data: %{name: "quote"}} = interaction) do
    Commands.RandomQuote.handle(interaction)
  end

  defp handle_interaction_creation(%Interaction{data: %{name: "clear"}} = interaction) do
    Commands.ClearVentingChannel.handle(interaction)
  end
end
