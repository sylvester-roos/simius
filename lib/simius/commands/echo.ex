defmodule Simius.Commands.Echo do
  require Logger

  alias Nostrum.Api

  def command do
    %{
      name: "echo",
      description: "Repeats your message.",
      options: [
        %{
          type: 3,
          name: "message",
          description: "The message for the bot to repeat.",
          required: true
        }
      ]
    }
  end

  def handle(interaction) do
    Logger.info("Echo called")

    echo =
      interaction.data.options
      |> Enum.find(fn %{name: "message"} -> true end)
      |> Map.get(:value)

    # https://discord.com/developers/docs/interactions/receiving-and-responding#interaction-response-object
    response = %{
      type: 4,
      data: %{
        content: echo
      }
    }

    Api.create_interaction_response!(interaction, response)
  end
end
