defmodule Simius.Commands.ClearVentingChannel do
  require Logger

  alias Simius.{Persistence, VentingClearer}
  alias Nostrum.Api

  def command do
    %{
      name: "clear",
      description: "Clears the venting channel on the set interval.",
      min_options: 1,
      options: [
        %{
          # INTEGER
          type: 4,
          name: "days",
          description: "Number of days between clears",
          required: true
        },
        %{
          type: 4,
          name: "hours",
          description: "Number of hours between clears",
          required: true
        },
        %{
          type: 4,
          name: "minutes",
          description: "Number of minutes between clears",
          required: true
        },
        %{
          type: 4,
          name: "seconds",
          description: "Number of seconds between clears",
          required: true
        }
      ]
    }
  end

  def handle(interaction) do
    Logger.info("ClearVentingChannel called")

    interval =
      interaction.data.options
      |> Enum.map(fn
        %{name: "days", value: value} -> Duration.new!(day: value)
        %{name: "hours", value: value} -> Duration.new!(hour: value)
        %{name: "minutes", value: value} -> Duration.new!(minute: value)
        %{name: "seconds", value: value} -> Duration.new!(second: value)
      end)
      |> Enum.reduce(&Duration.add/2)
      |> max(Duration.new!(second: 10))
      # keep a safe margin for the 14 day bulk delete limit
      |> min(Duration.new!(day: 13, hour: 12))
      |> to_timeout()

    Persistence.save_interval(interval)
    VentingClearer.new_timer()

    time_parts = VentingClearer.formatted_interval()

    response = %{
      type: 4,
      data: %{
        content:
          "⏰ Interval saved. The channel will be cleared immediately and then every #{time_parts}."
      }
    }

    Api.create_interaction_response!(interaction, response)
  end
end
