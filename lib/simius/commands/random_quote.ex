defmodule Simius.Commands.RandomQuote do
  require Logger

  import Nostrum.Struct.Embed

  alias Nostrum.Api

  defp quotes_channel_id do
    Application.fetch_env!(:simius, :quotes_channel_id)
    |> String.to_integer()
  end

  def command do
    %{
      name: "quote",
      description: "Reads a random quote from the quotes channel."
    }
  end

  def handle(interaction) do
    Logger.info("RandomQuote called")

    embed =
      interaction.guild_id
      |> get_random_quote_message()
      |> create_embed()

    response = %{
      type: 4,
      data: %{
        embeds: [embed]
      }
    }

    Api.create_interaction_response!(interaction, response)
  end

  defp get_random_quote_message(_guild_id) do
    quote_pattern = ~r([\x{201C}\x{201D}\x{201E}\x{201F}"].*?[\x{201C}\x{201D}\x{201E}\x{201F}"])u

    Api.get_channel!(quotes_channel_id())
    |> Map.fetch!(:id)
    |> Api.get_channel_messages!(:infinity)
    |> Stream.filter(fn message ->
      quote? = message.content =~ quote_pattern
      has_attachment? = message |> has_attachment?()

      [
        quote?,
        has_attachment?
      ]
      |> Enum.any?(fn message -> message end)
    end)
    |> Enum.random()
  end

  defp has_attachment?(message), do: message.attachments != []

  defp create_embed(message) do
    embed =
      %Nostrum.Struct.Embed{}
      |> put_author(
        "Recorded by #{message.author.username}",
        nil,
        Nostrum.Struct.User.avatar_url(message.author)
      )
      |> put_description(message.content)

    message.attachments
    |> Enum.reduce(embed, &put_image(&2, &1.url))
  end
end
