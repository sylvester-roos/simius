defmodule Simius.Consumer.Ready do
  require Logger

  alias Simius.Commands
  alias Nostrum.Api

  defp status, do: Application.fetch_env!(:simius, :status)
  defp guild_id, do: Application.fetch_env!(:simius, :guild_id)

  defp commands do
    [
      Commands.Echo.command(),
      Commands.RandomQuote.command(),
      Commands.ClearVentingChannel.command()
    ]
  end

  def handle(data) do
    commands()
    |> List.flatten()
    |> Enum.each(fn command ->
      {:ok, command} = Api.create_guild_application_command(guild_id(), command)
      Logger.info("Registered command #{inspect(command.name)}")
    end)

    Logger.info("Logged in and ready, seeing #{length(data.guilds)} guild(s)")

    :ok = Api.update_status(:online, status(), 1)
  end
end
