{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nix2container = {
      url = "github:nlewo/nix2container";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      nix2container,
    }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      nix2containerPkgs = nix2container.packages.${system};

      simius = import ./package.nix { inherit pkgs; };
      container = import ./container.nix {
        inherit pkgs;
        inherit simius;
        inherit (nix2containerPkgs) nix2container;
      };
    in
    {
      inherit simius;
      packages.${system}.container = container;
    };
}
