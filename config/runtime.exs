import Config

config :nostrum,
  token: System.get_env("BOT_TOKEN")

config :simius,
  status: System.fetch_env!("STATUS"),
  guild_id: System.fetch_env!("GUILD_ID"),
  quotes_channel_id: System.fetch_env!("QUOTES_CHANNEL_ID"),
  venting_channel_id: System.fetch_env!("VENTING_CHANNEL_ID")
