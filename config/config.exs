import Config

config :nostrum,
  gateway_intents: [
    :guilds,
    :guild_messages,
    :message_content,
    :guild_voice_states
  ],
  youtubedl: nil,
  ffmpeg: nil

import_config "#{config_env()}.exs"
