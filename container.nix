{
  pkgs,
  nix2container,
  simius,
}:

# TODO: figure out why simius gets rebuilt even when it isn't touched
let
  minimalLocales = pkgs.glibcLocales.override {
    allLocales = false;
    locales = [ "en_US.UTF-8/UTF-8" ];
  };
in
nix2container.buildImage {
  name = "simius";
  config = {
    env = [
      "LOCALE_ARCHIVE=${minimalLocales}/lib/locale/locale-archive"
      "LANG=en_US.UTF-8"
      "LANGUAGE=en_US:en"
      "LC_ALL=en_US.UTF-8"
    ];
    entrypoint = [ "${simius}/bin/simius" ];
    cmd = [ "start" ];
  };
  layers = [
    (nix2container.buildLayer {
      # a list of derivations copied to the image root directory
      copyToRoot = [
        (pkgs.buildEnv {
          name = "busybox";
          paths = with pkgs; [
            # a `sh` is required
            busybox # includes `sh` and many other utils without wasting much space
          ];
          pathsToLink = [ "/bin" ];
        })
      ];
    })
  ];
}
