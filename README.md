# Simius

This is a personal Discord bot. It uses Elixir and Nostrum.

## Development

1. Put the following in a `.env.dev` file, replacing the empty strings with the correct values:

```sh
BOT_TOKEN=
STATUS=
GUILD_ID=
QUOTES_CHANNEL_ID=
VENTING_CHANNEL_ID=

# needed for mixRelease. Set this to some random value
RELEASE_COOKIE=
```

2. Install Nix, Devenv, and Direnv
3. Run `direnv allow`

### Building the container image

I use Podman instead of Docker, so you might need to adapt the command

```sh
# use the script included in `devenv.nix`
$ build-image
```
