{ pkgs }:

let
  beamPkgs = pkgs.beam.packagesWith pkgs.beam.interpreters.erlang_27;
  elixir = beamPkgs.elixir_1_18;

  pname = "simius";
  version = "0.1.0";
  src = ./.;

  mixFodDeps = beamPkgs.fetchMixDeps {
    pname = "mix-deps-${pname}";
    inherit src version;
    hash = "sha256-f5lYBn3CjE2qyRe2ysNr/nM1qW+5fgD466jmI+nmUCA=";
  };

  stripDebug = true;
in
beamPkgs.mixRelease {
  inherit
    pname
    version
    src
    mixFodDeps
    elixir
    stripDebug
    ;
}
